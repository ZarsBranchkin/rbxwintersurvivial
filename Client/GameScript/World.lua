local modules
local rand = math.random

local World = {}

  local base = game.Workspace.BasePlate
  local structures = {}
  local bushAmount = 20

  local function isEmpty( pos )
    local ray = Ray.new( pos+Vector3.new( 0, 50, 0 ), Vector3.new( 0, -50, 0 ) )
    local part = game.Workspace:FindPartOnRay( ray, game.Workspace.Snow )
    return part == nil
  end

  function World.Init()
    modules = _G.modules
  end

  function World.Generate()
    for i=1,bushAmount do
      local bush = {}
      bush.Model = game.ReplicatedStorage.Structures.Bush:clone()
      bush.Model.Parent = game.Workspace

      local size = base.Size
      local pos

      local p = 0
      repeat
        pos = base.Position + Vector3.new( size.x*(rand()-0.5)/5, size.y/2, size.z*(rand()-0.5)/5 )
      until isEmpty( pos )

      bush.Model:SetPrimaryPartCFrame( CFrame.new(pos) )
      table.insert( structures, bush )
    end
  end

  function World.Clear()
    for num, obj in ipairs( structures ) do
      obj.Model:Destroy()
    end
    structures = {}
  end

return World
