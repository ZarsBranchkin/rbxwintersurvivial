local controls = {}

controls.Space = "Interact"

controls.W = "Up"
controls.S = "Down"
controls.A = "Left"
controls.D = "Right"
controls.Up = "Up"
controls.Down = "Down"
controls.Left = "Left"
controls.Right = "Right"

return controls
