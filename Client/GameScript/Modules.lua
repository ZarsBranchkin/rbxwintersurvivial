local modules = script.Parent:GetChildren()
local loaded = {}

for num, module in ipairs( modules ) do
  if module:IsA( "ModuleScript" ) and module ~= script then
    local hasLoaded = false
    local content = require( module )
    local mt = {}
    loaded[module.Name] = {}

    function mt.__index( self, index )
      if not hasLoaded then
        hasLoaded = true

        if content.Init and type( content.Init ) == "function" then
          content.Init()
        end
      end

      return content[index]
    end

    mt.__newindex = content

    setmetatable( loaded[module.Name], mt )
  end
end
_G.modules = loaded

local mt = {}

function mt.__call( self, module )
  -- Hacky manual load
  local a = self[module].__load
end

setmetatable( loaded, mt )

return loaded
