local Inventory = {}

  local items = {}

  function Inventory.AddItem( name, amount )
    local newItem

    for i=1,#items do
      local item = items[i]
      if item.Name == name then
        newItem = item
        break
      end
    end

    if newItem then
      newItem.Amount = newItem.Amount + amount
    else
      newItem = {}
      newItem.Name = name
      newItem.Amount = tonumber(amount) or 1
      table.insert( items, newItem )
    end

    return newItem.Amount or 0
  end

  function Inventory.RemoveItem( name, amount )
    for i=1,#items do
      local item = items[i]
      local remove = false

      if item.Name == name then
        item.Amount = item.Amount - amount
        if item.Amount <= 0 then
          remove = true
        end
      end

      if remove then
        table.remove( items, i )
      end

      return item.Amount
    end
    return 0
  end

  function Inventory.GetItemCount( name )
    for i=1,#items do
      local item = items[i]
      if item.Name == name then
        return item.Amount
      end
    end
    return 0
  end

  function Inventory.Reset()
    items = {}
  end

return Inventory
