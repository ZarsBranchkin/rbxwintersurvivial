local Timer = {}

  local timers = {}

  local function findTimer( name )
    for i=1,#timers do
      if timers[i].Name == name then
        return timers[i]
      end
    end
  end

  function Timer.New( name, frequency, func, noAutoStart )
    local timer = {}
    timer.Name = name
    timer.Frequency = frequency
    timer.Function = func
    timer.Running = not noAutoStart
    timer.LastCall = 0

    table.insert( timers, timer )
  end

  function Timer.Start( name )
    local timer = findTimer( name )
    if timer then
      timer.Running = true
    end
  end

  function Timer.Stop( name )
    local timer = findTimer( name )
    if timer then
      timer.Running = false
    end
  end

  function Timer.Destroy( name )
    for i=1,#timers do
      if timers[i].Name == name then
        timers[i] = timers[#timers]
        timers[#timers] = nil
      end
    end
  end

  function Timer.Update( dt, t )
    for i=1,#timers do
      local timer = timers[i]
      if timer.Running and t - timer.LastCall >= timer.Frequency then
        local timerDt = t - timer.LastCall
        timer.LastCall = t
        timer.Function( timerDt,  t )
      end
    end
  end

return Timer
