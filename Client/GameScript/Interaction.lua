local modules

local Interaction = {}

  -- Constants
  local maxInterDist = 6

  local player = game.Players.LocalPlayer
  local mouse = player:GetMouse()
  local functions = require( script.Functions )
  local interactions = {}
  local target
  local interpret
  local interactProgress = 0
  local interacting = nil

  function getArgs( model )
    local args = {}
    for num, arg in ipairs( model.Function:GetChildren() ) do
      if arg.ClassName:find( "Value" ) then
        args[arg.Name] = arg.Value
      end
    end
    return args
  end

  function interact()
    if target then
      if interacting then
        interacting = nil
        modules.Character.UnfreezeMovement()
      else
        local ok = true
        local fun = functions[target.Interact.Function.Value]
        if fun.Start then
          local args = getArgs( target.Interact )
          ok = fun.Start( modules, target, args )
        end

        if ok then
          if target.Interact:FindFirstChild( "Duration" ) then
            interacting = target
            modules.Character.FreezeMovement()
            interactProgress = 0
            modules.HUD.InteractInfo( "Cancel" )
          else
            finishInteraction()
          end
        else
          print( "Error" )
        end
      end
    end
  end

  function finishInteraction()
    local command = target.Interact
    local args = getArgs( command )
    functions[target.Interact.Function.Value].Done( modules, target, args )

    interacting = nil
    modules.Character.UnfreezeMovement()
    modules.HUD.InteractProgress( 0 )
  end

  function findInteractions()
    local recurse
    function recurse( model )
      for num,obj in ipairs( model:GetChildren() ) do
        if obj:IsA( "Model" ) then
          if obj:FindFirstChild( "Interact" ) then
            table.insert( interactions, obj )
          end

          recurse( obj )
        end
      end
    end
    recurse( game.Workspace )

    game.Workspace.DescendantAdded:connect(function( child )
        if child:IsA( "Model" ) then
          if child:FindFirstChild( "Interact" ) then
            table.insert( interactions, child )
          end

          recurse( child )
        end
    end)
  end

  function updateInteractions( dt, t )
    if not interacting and player.Character and player.Character:FindFirstChild( "HumanoidRootPart" ) then
      local p = player.Character.HumanoidRootPart.CFrame
      local bestMatch
      local bestResult

      local deleted = 0
      for i=1,#interactions do
        local inter = interactions[i-deleted]

        if inter.Parent and inter.Interact.Value then
          local t = inter:GetPrimaryPartCFrame()
          local dir = t.p - p.p

          if dir.magnitude <= maxInterDist then
            local uDir = dir.unit
            local lv = p.lookVector
            lv = Vector2.new( lv.x, lv.z ).unit
            local dot = lv.x*uDir.x + lv.y*uDir.z
            local x,y = dir.x,dir.y
            local res = (x*x + y*y)^0.5 / dot

            if dot >= 0.4 then
              if not bestResult or bestResult > res then
                bestResult = res
                bestMatch = inter
              end
            end
          end
        else
          local n = #interactions
          interactions[i-deleted] = interactions[n]
          interactions[n] = nil
          deleted = deleted + 1
        end
      end

      target = bestMatch

      if target then
        local name = target.Interact.Function.Value
        if name and functions[name] then
          modules.HUD.InteractInfo( functions[name].Description )
        else
          modules.HUD.InteractHide()
        end
      else
          modules.HUD.InteractHide()
      end
    end
  end

  function Interaction.Init()
    modules = _G.modules

    findInteractions()
    modules.Input.RegisterCommand( "Interact", interact )
    modules.Timer.New( "Interaction", 1/20, updateInteractions )
  end

  function Interaction.Update( dt, t )
    if interacting then
      interactProgress = interactProgress + dt

      local duration = interacting.Interact:FindFirstChild( "Duration" )
      if duration then
        modules.HUD.InteractProgress( interactProgress / duration.Value )
        if duration.Value <= interactProgress then
          finishInteraction()
        end
      end
    end
  end

return Interaction
