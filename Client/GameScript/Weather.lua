local modules
local floor = math.floor
local min = math.min
local max = math.max
local cos = math.cos
local pi = math.pi
local format = string.format

local Weather = {}

  -- Constants
  local gameSecond = (60*60*24)/(5*60) -- One game day equals 5 real minutes
  local defaultGameTime = 12*60*60
  local nightTemperature = -1
  local dayTemperature = -0.5
  local weatherEncounterCooldown = 60*60*24
  local randomEncounterOffsetBase = 60*60*12
  local weatherEncounters = require( script.WeatherEncounters )

  -- Variables
  local gameTime = defaultGameTime
  local lighting = game.Lighting
  local lastUpdate = 0
  local lastWeatherEncounter = 0
  local tempModifier = 0
  local randomEncounterOffset = randomEncounterOffsetBase * math.random()
  local lastWeatherEncounterType
  local currentWeatherEncounter
  local encounterTimer = 0

  function Weather.Init()
    modules = _G.modules
  end

  function Weather.StartEncounter( name )
    local encounter
    if name and weatherEncounters[name] then
      encounter = weatherEncounters[name]
    else
      -- Temporary stuff
      if #weatherEncounters > 1 then
        repeat
          encounter = weatherEncounters[math.random( 1, #weatherEncounters )]
        until encounter ~= lastWeatherEncounterType
        lastWeatherEncounterType = encounter
      else
        encounter = weatherEncounters[1]
      end
    end

    if encounter.Begin then
      encounter.Begin()
    end

    if encounter.TemperatureModifier then
      tempModifier = encounter.TemperatureModifier
    end

    encounterTimer = 0
    currentWeatherEncounter = encounter
  end

  function Weather.Update( dt, t )
    gameTime = gameTime + gameSecond*dt
    local seconds = floor(gameTime)%60
    local minutes = floor(gameTime/60)%60
    local hours = floor(gameTime/3600)%24

    lighting.TimeOfDay = format( "%02i:%02i:%02i", hours, minutes, seconds )

    if currentWeatherEncounter  then
      encounterTimer = encounterTimer + gameSecond*dt

      if currentWeatherEncounter.Update then
        currentWeatherEncounter.Update( dt, t )
      end

      if currentWeatherEncounter.BaseDuration <= encounterTimer then
        if currentWeatherEncounter.End then
          currentWeatherEncounter.End()
        end

        tempModifier = 0
        lastWeatherEncounter = gameTime
        currentWeatherEncounter = nil
      end
    else
      if gameTime - lastWeatherEncounter >= weatherEncounterCooldown+randomEncounterOffset then
        randomEncounterOffset = randomEncounterOffsetBase * math.random()

        Weather.StartEncounter()
      end
    end

    if gameTime - lastUpdate >= 60*60 then
      local alpha = (-cos((gameTime/(24*60*60)) * 2*pi)+1)*0.5
      modules.Environment.SetGlobalTemperature( nightTemperature + (dayTemperature - nightTemperature)*alpha + tempModifier )
      lastUpdate = gameTime
    end
  end

  function Weather.Reset()
    if currentWeatherEncounter and currentWeatherEncounter.End then
      currentWeatherEncounter.End()
    end
    currentWeatherEncounter = nil
    lastWeatherEncounter = 0

    gameTime = defaultGameTime
    lastUpdated = 0
  end

return Weather
