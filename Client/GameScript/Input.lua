local is = game:GetService( "UserInputService" )

local Input = {}

  local commands = {}
  local defaults = { __index = require( script.DefaultControls ) }
  local controls = {}
  local commandState = {}
  local mouse = game.Players.LocalPlayer:GetMouse()
  setmetatable( controls, defaults )

  function Input.Init()
    is.InputBegan:connect( onInput )
    is.InputChanged:connect( onInput )
    is.InputEnded:connect( onInput )

    Input.MouseSensitivity = 0.1
    mouse.TargetFilter = game.Workspace.CurrentCamera
  end

  function Input.RegisterCommand( command, began, changed, ended ) -- Pass true to changed/ended to use same function as began
    commands[command] = {
      Begin = began,
      Change = changed == true and began or changed,
      End = ended == true and began or changed }
  end

  function Input.GetCommandState( command )
    return commandState[ command ] or false
  end

  function Input.ResetDelta()
    local command =  controls["MouseX"]
    if command then
      commandState[ command ] = 0

      if commands[command] and commands[command].End then
        commands[command].End()
      end
    end

    command =  controls["MouseY"]
    if command then
      commandState[ command ] = 0

      if commands[command] and commands[command].End then
          commands[command].End()
        end
    end
  end

  function Input.LockMouse()
    is.MouseBehavior = Enum.MouseBehavior.LockCenter
  end

  function Input.SetMouseSensitivity( sens )
    Input.MouseSensitivity = sens
  end

  function onInput( input )
    if input.UserInputType == Enum.UserInputType.Keyboard or input.UserInputType == Enum.UserInputType.Gamepad1 then
      local command = controls[input.KeyCode.Name]
      local state = input.UserInputState.Name

      if command then
        commandState[ command ] = (state == Enum.UserInputState.Begin.Name or state == Enum.UserInputState.Change.Name)

        if commands[command] and commands[command][state] and not is:GetFocusedTextBox() then
          commands[command][state]()
        end
      end
    elseif input.UserInputType == Enum.UserInputType.MouseMovement or input.UserInputType == Enum.UserInputType.MouseWheel then
      local dx, dy, dw = input.Delta.x,  input.Delta.y,  input.Position.z
      local state = input.UserInputState.Name

      local command = controls[ "MouseX" ]
      if command then
        commandState[ command ] = dx

        if commands[command] and commands[command][state] then
          commands[command][state]()
        end
      end

      command =  controls[ "MouseY" ]
      if command then
        commandState[ command ] = dy

        if commands[command] and commands[command][state] then
          commands[command][state]()
        end
      end

      command = controls[ "ScrollWheel" ]
      if command then
        commandState[ command ] = dw

        if commands[command] and commands[command][state] then
          commands[command][state]()
        end
      end
    end
  end

return Input
