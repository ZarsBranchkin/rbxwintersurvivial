local Functions = {}

  function Functions.Describe( funName )
    return Functions["info__" .. funName]
  end

  local gatherWood = {}
  gatherWood.Description = "Gather wood"
  function gatherWood.Done( modules, model, args )
    local total = modules.Inventory.AddItem( "Wood", args.Amount )
    modules.HUD.UpdateBranches( total )
    model:Destroy()
  end
  Functions.gatherWood = gatherWood

  local addWood = {}
  addWood.Description = "Add wood"
  function addWood.Done( modules, model, args )
    local value = tonumber(args.Amount) or 1
    local woodAmount = modules.Inventory.GetItemCount( "Wood" )
    if woodAmount >= value then
      modules.Inventory.RemoveItem( "Wood", value )
      modules.Home.AddWood( value )
      modules.HUD.UpdateBranches( woodAmount-value )
    else
      print( "Not enough wood" )
    end
  end
  Functions.addWood = addWood

  local fixHouse = {}
  fixHouse.Description = "Fix house"
  function fixHouse.Start( modules, model, args )
    if args.Amount then
      if modules.Inventory.GetItemCount( "Wood" ) >= args.Amount then
        return true
      end
    else
      return true
    end

    return false
  end
  function fixHouse.Done( modules, model, args )
    local left = modules.Inventory.RemoveItem( "Wood", args.Amount )
    modules.HUD.UpdateBranches( left )
    modules.Home.PatchHole( model )
  end
  Functions.fixHouse = fixHouse

  function Functions.random( a, b )
    return math.random( tonubmer(a), tonumber(b) )
  end

return Functions
