local modules

local Home = {}

  local temperature
  local fireplace = game.Workspace.Home.Fireplace
  local fireplaceFuel = 0
  local woodFuelRatio = 10
  local burnSpeed = 0.5
  local homeEnv

  function fireplaceOn()
    -- fireplace.Fire.Material = Enum.Material.Neon
    -- fireplace.Fire.BrickColor = BrickColor.new( "Neon orange" )
    fireplace.v1fire.Body.Light.Brightness = 2
    fireplace.FireFake.Fire.Fire.Enabled = true
    fireplace.FireFake.Particles.ParticleEmitter.Enabled = true
    fireplace.Chimney.Smoke.Enabled = true
  end

  function fireplaceOff()
    -- fireplace.Fire.Material = Enum.Material.Metal
    -- fireplace.Fire.BrickColor = BrickColor.new( "Dark taupe" )
    -- fireplace.Fire.Light.Brightness = 0
    fireplace.v1fire.Body.Light.Brightness = 0
    fireplace.FireFake.Fire.Fire.Enabled = false
    fireplace.FireFake.Particles.ParticleEmitter.Enabled = false
    fireplace.Chimney.Smoke.Enabled = false
  end

  function Home.Init()
    modules = _G.modules
  end

  function Home.Reset()
    temperature = modules.Environment.GetGlobalTemperature() * 1.1
    if homeEnv then
      homeEnv:Destroy()
    end
    homeEnv = modules.Environment.New( "Home", {game.Workspace.Home.HomeEnv}, temperature )
    fireplaceFuel = 0
  end

  function Home.PatchHole( model )
    model.Interact.Value = false
    local parts = model:GetChildren()

    for i=1,#parts do
      local part = parts[i]
      if part:IsA( "BasePart" ) then
        part.Transparency = 0
        part.CanCollide = true
      end
    end
  end

  function Home.Update( dt, t )
    if fireplaceFuel > 0 then
      fireplaceFuel = fireplaceFuel - burnSpeed * dt
      if fireplaceFuel <= 0 then
        homeEnv.Temperature = modules.Environment.GetGlobalTemperature() + 0.5
        fireplaceFuel = 0
        fireplaceOff()
      elseif fireplaceFuel <= 10 then
        homeEnv.Temperature = 5
        fireplaceOn()
      else
        homeEnv.Temperature = 10
        fireplaceOn()
      end
    end
  end

  function Home.AddWood( amount )
    fireplaceFuel = fireplaceFuel + amount * woodFuelRatio
  end

return Home
