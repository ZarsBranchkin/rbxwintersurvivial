local modules

local Camera = {}

  local player = game.Players.LocalPlayer
  local cam = game.Workspace.CurrentCamera
  local renderLoop

  function Camera.Init()
    modules = _G.modules
  end

  function Camera.CharacterCamera()
    local character = player.Character
    cam.CameraType = Enum.CameraType.Scriptable
    cam.FieldOfView = 40

    local offset = Vector3.new( 0, 40, 10 )
    local lastPos = character.HumanoidRootPart.Position
    local getRenderCF = character.HumanoidRootPart.GetRenderCFrame

    renderLoop = function( dt, t )
      if character:FindFirstChild( "HumanoidRootPart" ) then
        local pos = getRenderCF( character.HumanoidRootPart ).p
        cam.CFrame = CFrame.new( pos + offset, pos )
      end
    end
  end

  function Camera.Render( dt, t )
    if renderLoop then
      renderLoop( dt, t )
    end
  end

return Camera
