local modules

local Environment = {}

  local player = game.Players.LocalPlayer
  local workspace = game.Workspace
  local findPartsInRegion3 = workspace.FindPartsInRegion3
  local findPartOnRay = workspace.FindPartOnRay
  local currentEnvironment
  local environments = {}
  local lastUpdate = 0
  local updateFrequency = 1
  local globalTemperature = -5

  function updateEnvironment()
    local char = player.Character
    if char and char:FindFirstChild( "Torso" ) then
      local pos = char.Torso.Position
      local done = false

      for i=1,#environments do
        local env = environments[i]

        for p=1,#env.Parts do
          local part = env.Parts[p]
          local size = part.Size
          local pPos = part.Position
          if (pPos - pos).magnitude <= size.magnitude/2 then
            local region = Region3.new( pos-Vector3.new( 0.1, 0.1, 0.1 ), pos+Vector3.new( 0.1, 0.1, 0.1 ) )
            local parts = findPartsInRegion3( workspace, region, char )
            local found = false
            for i=1,#parts do
              local fPart = parts[i]
              if part == fPart then
                found = true
              end
            end

            if found then
              currentEnvironment = env
              done = true
              break
            end
          end
        end

        if done then
          break
        end
      end

      if done then
        modules.Character.SetTemperature( currentEnvironment.Temperature )
        modules.HUD.UpdateEnvironmentTemperature( currentEnvironment.Temperature )
      else
        currentEnvironment = nil
        modules.Character.SetTemperature( globalTemperature )
        modules.HUD.UpdateEnvironmentTemperature( globalTemperature )
      end
    end
  end

  function Environment.Init()
    modules = _G.modules

    modules.Timer.New( "Environment", updateFrequency, updateEnvironment )
  end

  function Environment.New( name, envParts, temp )
    local env = {}
    env.Name = name
    env.Parts = envParts
    env.Temperature = temp or 0
    env.Destroy = destroyEnvironment

    table.insert( environments, env )
    return env
  end

  function Environment.GetCurrentEnvironment()
    return currentEnvironment
  end

  function Environment.SetGlobalTemperature( temp )
    globalTemperature = temp
    if not currentEnvironment then
      modules.Character.SetTemperature( temp )
    end
  end

  function Environment.GetGlobalTemperature()
    return globalTemperature
  end

  function destroyEnvironment( self )
    for i=1,#environments do
      local env = environments[i]

      if env == self then
        table.remove( environments, i )
        break
      end
    end
  end

return Environment
