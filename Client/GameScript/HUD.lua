local floor = math.floor

local HUD = {}

  local player = game.Players.LocalPlayer
  local ui
  local interInfoVisible = false
  local interFormat = "[SPACE] %s"
  local interUI

  function HUD.Init()
  end

  function HUD.Create()
    ui = game.ReplicatedStorage.HUD:Clone()
    ui.Parent = player.PlayerGui

    interUI = ui.Interact

    game.StarterGui:SetCoreGuiEnabled( Enum.CoreGuiType.All, false )
  end

  function HUD.UpdateTemperature( temp )
    ui.Temperature.Text = "Temperature: " .. floor( temp )
  end

  function HUD.UpdateEnvironmentTemperature( temp )
    ui.EnvTemperature.Text = "Env temperature: " .. floor( temp )
  end

  function HUD.UpdateBranches( branches )
    ui.Branches.Text = "Wood: " .. floor( branches )
  end

  function HUD.UpdateTime( t )
    ui.Time.Text = "Time alive: " .. floor( t )
  end

  function HUD.InteractInfo( str )
    interInfoVisible = true
    interUI.Text = string.format( interFormat, str )
    interUI.Visible = true
  end

  function HUD.InteractHide()
    if interInfoVisible then
      interInfoVisible = false
      interUI.Visible = false
    end
  end

  function HUD.InteractProgress( alpha )
    interUI.Bar.Progress.Size = UDim2.new( alpha, 0, 0, 4 )
  end

return HUD
