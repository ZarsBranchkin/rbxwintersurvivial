local modules
local character = {}

  -- Constants
  local defaultWalkspeed = 16

  local player = game.Players.LocalPlayer
  local timeAlive = 0
  local maxTemperature = 100
  local temperature
  local envTemperature
  local charModel

  function character.Init()
    modules = _G.modules

    player.CharacterAdded:connect( characterAdded )
    if player.Character then
      characterAdded( player.Character )
    end
  end

  function character.SetTemperature( mod )
    envTemperature = mod
  end

  function character.GetTemperature()
    return temperature
  end

  function character.FreezeMovement()
    if charModel and charModel:FindFirstChild( "Humanoid" ) then
      charModel.Humanoid.WalkSpeed = 0
    end
  end

  function character.UnfreezeMovement()
    if charModel and charModel:FindFirstChild( "Humanoid" ) then
      charModel.Humanoid.WalkSpeed = defaultWalkspeed
    end
  end

  function character.Update( dt, t )
    timeAlive = timeAlive + dt
    modules.HUD.UpdateTime( timeAlive )

    temperature = temperature + envTemperature * dt
    modules.HUD.UpdateTemperature( temperature )

    if temperature <= 0 then
      charModel.Humanoid.Health = 0
      temperature = 0
    elseif temperature > maxTemperature then
      temperature = maxTemperature
    end
  end

  function characterAdded( char )
    charModel = char
    temperature = maxTemperature
    envTemperature = 0
    timeAlive = 0

    modules.HUD.Create()
    modules.Home.Reset()
    modules.Inventory.Reset()
    modules.World.Clear()
    modules.World.Generate()
    modules.Weather.Reset()

    char.Humanoid.JumpPower = 0

    -- Ugly stuff
    spawn(function()
      char:WaitForChild( "HumanoidRootPart" )
      modules.Camera.CharacterCamera()
      wait(0.1)
      char.HumanoidRootPart.CFrame = game.Workspace.SpawnLocation.CFrame + Vector3.new( 0, 3, 0 )
    end)
  end

return character
