local WeatherEncounters = {}

local snow = {}
  snow.TemperatureModifier = -1
  snow.BaseDuration = 4*60*60

  function snow.Begin()
    game.Workspace.Snow.ParticleEmitter.Enabled = true
  end

  function snow.End()
    game.Workspace.Snow.ParticleEmitter.Enabled = false
  end
table.insert( WeatherEncounters, snow )

return WeatherEncounters
