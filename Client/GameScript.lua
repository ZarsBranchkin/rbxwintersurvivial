-- ClassName: LocalScript
local modules = require( script.Modules )
local player = game.Players.LocalPlayer

modules.Home.Reset()
modules( "Interaction" )

-- Update loop
local workspace = game.Workspace
local heartbeat = game:GetService( "RunService" ).Heartbeat
local lastTick = tick()
local t = 0
function Update()
  while true do
    heartbeat:wait()

    local newTick = tick()
    local dt = newTick - lastTick
    lastTick = newTick
    t = t + dt

    modules.Weather.Update( dt, t )
    modules.Home.Update( dt, t )
    modules.Character.Update( dt, t )
    modules.Interaction.Update( dt, t )
    modules.Timer.Update( dt, t )
  end
end
spawn( Update )

-- Render loop
local lastRenderTick = tick()
local tRender = 0

-- Will use local functions in render loop, to keep render loop quick
local cameraRender = modules.Camera.Render
function Render()
  local newTick = tick()
  local dt = newTick - lastTick
  lastRenderTick = newTick
  tRender = tRender + dt

  cameraRender( dt, tRender )
end
game:GetService( "RunService" ):BindToRenderStep( "MainRender", Enum.RenderPriority.Last.Value, Render )
